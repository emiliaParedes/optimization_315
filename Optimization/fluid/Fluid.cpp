
#include "Fluid.h"

#include "profiler.h"				//em
#include <immintrin.h>

profiler_::Profiler* prof = profiler_::Profiler::GetProfiler();	//em

// Zero the fluid simulation member variables for sanity
Fluid::Fluid()
{
	step = 0;
	paused = false;
	pause_step = 0xFFFFFFFF;

	width = 0;
	height = 0;
	grid_w = 0;
	grid_h = 0;

	gridindices = NULL;
	gridoffsets = NULL;
	num_neighbors = 0;
	// If this value is too small, ExpandNeighbors will fix it
	neighbors_capacity = 263 * 1200;
	neighbors = new FluidNeighborRecord[ neighbors_capacity ];

	// Precompute kernel coefficients
	// See "Particle-Based Fluid Simulation for Interactive Applications"
	// "Poly 6" Kernel - Used for Density
	poly6_coef = 315.0f / (64.0f * D3DX_PI * pow(FluidSmoothLen, 9));
	// Gradient of the "Spikey" Kernel - Used for Pressure
	grad_spiky_coef = -45.0f / (D3DX_PI * pow(FluidSmoothLen, 6));
	// Laplacian of the "Viscosity" Kernel - Used for Viscosity
	lap_vis_coef = 45.0f / (D3DX_PI * pow(FluidSmoothLen, 6));
}

// Destructor
Fluid::~Fluid() 
{
	Clear();
	delete[] gridoffsets; gridoffsets = NULL;
	num_neighbors = 0;
	neighbors_capacity = 0;
	delete[] neighbors; neighbors = neighbors;
}

// Create the fluid simulation
// width/height is the simulation world maximum size
void Fluid::Create(float w, float h) 
{
	////prof->StartFrame("CreateFluid");
	width = w;
	height = h;
	grid_w = (int)(width / FluidSmoothLen);
	grid_h = (int)(height / FluidSmoothLen);

	delete[] gridoffsets;
	gridoffsets = new FluidGridOffset[ grid_w * grid_h ];

	planes.resize(4);
	planes[0] = (D3DXVECTOR3(1, 0, 0));
	planes[1] = (D3DXVECTOR3(0, 1, 0));
	planes[2] = (D3DXVECTOR3(-1, 0, width));
	planes[3] = (D3DXVECTOR3(0, -1, height));
	////prof->EndFrame("CreateFluid");
}

// Fill a region in the lower left with evenly spaced particles
void Fluid::Fill(float size) 
{
	////prof->StartFrame("Fillfluid");
	Clear();

	int w = (int)(size / FluidInitialSpacing);

	//pre allocate memory for particles				//em
	//particles.reserve(w * w);						//em
#pragma region NEW SHIT 
	particles_pos.resize(w * w, {});
	particles_vel.resize(w * w, {});
	particles_acc.resize(w * w, {});
	particles_density.resize(w * w, {});
	particles_pressure.resize(w * w, {});
#pragma endregion 



	// Allocate
	gridindices = new unsigned int[w * w]{0};
	//for (int i = 0; i < w*w; i++)
	//{
	//	//Particle* p = new Particle;
	//	//particles.push_back(p);
	//	gridindices[i] = 0;		//em
	//}

	// Populate
	for (int x = 0; x < w; x++)
	{
		const float pos_x = x* FluidInitialSpacing;
		for (int y = 0; y < w; y++)
		{
			//Particle* part_at = particles[y * w + x];		//em
			particles_pos[y * w + x] = D3DXVECTOR2(pos_x, height - y * FluidInitialSpacing);		//em (acces memeber instead of calling function Height)
			//particles_vel[y * w + x] = D3DXVECTOR2(0, 0);
			//part_at->acc = D3DXVECTOR2(0, 0);
			//gridindices[ y*w+x ] = 0;			//em
		}
	}
	////prof->EndFrame("Fillfluid");
}

// Remove all particles
void Fluid::Clear() 
{
	////prof->StartFrame("Clearfluid");
	step = 0;

	//for (auto p : particles)
	//	delete p;
	//particles.clear();
	particles_pos.clear();
	particles_vel.clear();
	particles_acc.clear();
	particles_density.clear();
	particles_pressure.clear();

	delete[] gridindices; gridindices = NULL;
	////prof->EndFrame("Clearfluid");
}

// Expand the Neighbors list if necessary
// This function is rarely called
void Fluid::ExpandNeighbors() 
{
	//prof->StartFrame("ExpandNeighbors");
	// Increase the size of the neighbors array because it is full
	neighbors_capacity += 20;
	FluidNeighborRecord* new_neighbors = new FluidNeighborRecord[ neighbors_capacity ];
	memcpy( new_neighbors, neighbors, sizeof(FluidNeighborRecord) * num_neighbors );
	delete[] neighbors;
	neighbors = new_neighbors;
	//prof->EndFrame("ExpandNeighbors");
}

// Simulation Update
// Build the grid of neighbors
// Imagine an evenly space grid.  All of our neighbors will be
// in our cell and the 8 adjacent cells
void Fluid::UpdateGrid() 
{
	//prof->StartFrame("UpdateGridFluid");
	// Cell size is the smoothing length

	// Clear the offsets
	const int size = (grid_w * grid_h);				//em 
	

	for( int offset = 0; offset < size; offset++ ) 	//em
	{
		gridoffsets[offset].count = 0;
	}

	// Count the number of particles in each cell
	//constants 
	const int grid_w_1 = grid_w - 1, grid_h_1 = grid_h - 1;
	const float fluid_delta = 1.0 / FluidSmoothLen;

	for( unsigned int particle = 0; particle < particles_pos.size(); particle++ )
	{
		// Find where this particle is in the grid
		//Particle* part = particles[particle];			//em
		const int p_gx = min(max((int)(particles_pos[particle].x * (fluid_delta)), 0), grid_w_1);
		const int p_gy = min(max((int)(particles_pos[particle].y * (fluid_delta)), 0), grid_h_1);
		const int cell = p_gy * grid_w + p_gx ;
		gridoffsets[ cell ].count++;
	}

	// Prefix sum all of the cells
	unsigned int sum = 0;

	for( int offset = 0; offset < size; offset++ ) 
	{
		FluidGridOffset& fgo = gridoffsets[offset];
		fgo.offset = sum;
		sum += fgo.count;
		fgo.count = 0;
	}

	// Insert the particles into the grid
	for( unsigned int particle = 0; particle < particles_pos.size(); particle++ )
	{
		// Find where this particle is in the grid
		//Particle* part = particles[particle];			//em
		const D3DXVECTOR2 pos = particles_pos[particle];
		const int p_gx = min(max((int)(pos.x * (fluid_delta)), 0), grid_w_1);
		const int p_gy = min(max((int)(pos.y * (fluid_delta)), 0), grid_h_1);
		const int cell = p_gy * grid_w + p_gx ;

		FluidGridOffset& fgo = gridoffsets[cell];
		gridindices[ fgo.offset + fgo.count ] = particle;
		fgo.count++;
	}
	//prof->EndFrame("UpdateGridFluid");
}

// Simulation Update
// Build a list of neighbors (particles from adjacent grid locations) for every particle
void Fluid::GetNeighbors() 
{
	//prof->StartFrame("GetNeighbors");
	// Search radius is the smoothing length
	const float h2 = FluidSmoothLen*FluidSmoothLen;

	num_neighbors = 0;
	const float fluid_delta = 1.0f / FluidSmoothLen;
	const int g_w = grid_w - 1;
	const int g_h = grid_h - 1;

	for( unsigned int P = 0; P < particles_pos.size(); P++ )
	{
		//Particle& part = *particles[P];			//em
		// Find where this particle is in the grid
		const D3DXVECTOR2& pos_p = particles_pos[P];
		const int p_gx = min(max((int)(pos_p.x * (fluid_delta)), 0), g_w);
		const int p_gy = min(max((int)(pos_p.y * (fluid_delta)), 0), g_h);
		const int cell = p_gy * grid_w + p_gx ;

		// For every adjacent grid cell (9 cells total for 2D)
		const int end_d_gy = ((p_gy < g_h) ? 1 : 0);		//em
		const int end_d_gx = ((p_gx < g_w) ? 1 : 0);		//em

		for (int d_gy = ((p_gy<1)?0:-1); d_gy <= end_d_gy; d_gy++)
		{
			for (int d_gx = ((p_gx<1)?0:-1); d_gx <= end_d_gx; d_gx++)
			{
				// Neighboring cell
				const int n_cell = cell + d_gy * grid_w + d_gx; 

				// Loop over ever particle in the neighboring cell
				unsigned int* start = gridindices + gridoffsets[n_cell].offset;		//em
				const unsigned int* end = start + gridoffsets[n_cell].count;

				unsigned counter = start - end;				//em
				for(; counter > 0; counter++, ++start)		//em
				//for ( ; start != end ; ++start) 
				{
					const unsigned int N = *start;
					// Only record particle "pairs" once
					if (P > N) 
					{
						const D3DXVECTOR2 pos_P = pos_p;		//em
						// Distance squared
						const D3DXVECTOR2 d = pos_P - particles_pos[N];	//em
						const float distsq = d.x * d.x + d.y * d.y;

						// Check that the particle is within the smoothing length
						if (distsq < h2) 
						{
							if ( num_neighbors >= neighbors_capacity ) 
							{
								ExpandNeighbors();
							}
							// Record the ID of the two particles
							// And record the squared distance
							FluidNeighborRecord& record = neighbors[ num_neighbors ];
							record.p = P;
							record.n = N;
							record.distsq = distsq;
							num_neighbors++;
						}
					}
				}
			}
		}
	}
	//prof->EndFrame("GetNeighbors");
}

// Simulation Update
// Compute the density for each particle based on its neighbors within the smoothing length
void Fluid::ComputeDensity() 
{
	//prof->StartFrame("ComputeDensityFluid");


	const float fluid_sm_sq = FluidSmoothLen * FluidSmoothLen;									//em 
	const float addition = (fluid_sm_sq) * (fluid_sm_sq) * (fluid_sm_sq) * FluidWaterMass;		//em
	__m128 add = { addition };
	for( unsigned int particle = 0; particle < particles_pos.size(); particle+=4 )
	{
		if (particle + 4 > particles_pos.size())
			particle = particles_pos.size() - 4;
			//	particle = particles_pos.size() - 8;
		_mm_store_ps1(&(particles_density[0]) + particle, add);
		// This is r = 0
		//particles_density[particle] = addition;
	}
	// foreach neighboring pair of particles
	for( unsigned int i = 0; i < num_neighbors ; i++ ) 
	{		
		FluidNeighborRecord& neigh = neighbors[i];
		// distance squared
		
		// Density is based on proximity and mass
		// Density is:
		// M_n * W(h, r)
		// Where the smoothing kernel is:
		// The the "Poly6" kernel
		const float h2_r2 = fluid_sm_sq - neigh.distsq;
		const float dens = h2_r2*h2_r2*h2_r2;
		neigh.distsq = sqrt(neigh.distsq);

		//float P_mass = FluidWaterMass;		//em
		//float N_mass = FluidWaterMass;
		 
		const float sum = FluidWaterMass * dens;
		particles_density[neigh.p]+= sum;
		particles_density[neigh.n]+= sum;
	}
	
	// Approximate pressure as an ideal compressible gas
	// based on a spring eqation relating the rest density

	for( unsigned int particle = 0 ; particle < particles_pos.size(); ++particle )
	{
		//Particle* part = particles[particle];
		float& density = particles_density[particle];
		density *= poly6_coef;
		particles_pressure[particle] = FluidStiff * max(pow(density/ FluidRestDensity, 3) - 1, 0);
	}

	//add = { poly6_coef };
	//for (unsigned int particle = 0; particle < particles_pos.size(); particle += 4)
	//{
	//	if (particle + 4 > particles_pos.size())
	//		particle = particles_pos.size() - 4;
	//	//Particle* part = particles[particle];
	//	//float& density = particles_density[particle];
	//	__m128 new_density{ particles_density[particle],
	//						particles_density[particle + 1],
	//						particles_density[particle + 2],
	//						particles_density[particle + 3] };
	//	new_density = _mm_mul_ps(new_density, add);
	//	_mm_store_ps1(&(particles_density[0]) + particle, new_density);
	//
	//	//density *= poly6_coef;
	//	particles_pressure[particle] = FluidStiff * max(pow(particles_density[particle] / FluidRestDensity, 3) - 1, 0);
	//	particles_pressure[particle + 1] = FluidStiff * max(pow(particles_density[particle + 1] / FluidRestDensity, 3) - 1, 0);
	//	particles_pressure[particle + 2] = FluidStiff * max(pow(particles_density[particle + 2] / FluidRestDensity, 3) - 1, 0);
	//	particles_pressure[particle + 3] = FluidStiff * max(pow(particles_density[particle + 3] / FluidRestDensity, 3) - 1, 0);
	//}

	//prof->EndFrame("ComputeDensityFluid");
}

// Simulation Update
// Perform a batch of sqrts to turn distance squared into distance
//__forceinline void Fluid::SqrtDist()
//{
//	//prof->StartFrame("SqrtDistFluid");
//	for( unsigned int i = 0; i < num_neighbors; i++ ) 
//	{
//		neighbors[i].distsq = sqrt(neighbors[i].distsq);
//	}
//	//prof->EndFrame("SqrtDistFluid");
//}

// Simulation Update
// Compute the forces based on the Navier-Stokes equations for laminer fluid flow
// Follows is lots more voodoo
void Fluid::ComputeForce() 
{
	//prof->StartFrame("ComputeForceFluid");
	const float fluid_viscosity_and_coef =  FluidViscosity* lap_vis_coef;

	D3DXVECTOR2 force{};
	// foreach neighboring pair of particles
	for( unsigned int i = 0; i < num_neighbors; i++ ) 
	{				
		// Compute force due to pressure and viscosity
		FluidNeighborRecord& fnr = neighbors[i];
		float h_r = FluidSmoothLen - fnr.distsq;
		//Particle* part_n = particles[fnr.n];				//em
		//Particle* part_p = particles[fnr.p];				//em
		const D3DXVECTOR2 diff = particles_pos[fnr.n] - particles_pos[fnr.p];

		// Forces is dependant upon the average pressure and the inverse distance
		// Force due to pressure is:
		// 1/rho_p * 1/rho_n * Pavg * W(h, r)
		// Where the smoothing kernel is:
		// The gradient of the "Spikey" kernel
		force = (0.5f * (particles_pressure[fnr.p]+ particles_pressure[fnr.n])* grad_spiky_coef * h_r / fnr.distsq ) * diff;
		
		// Viscosity is based on relative velocity
		// Viscosity is:
		// 1/rho_p * 1/rho_n * Vrel * mu * W(h, r)
		// Where the smoothing kernel is:
		// The laplacian of the "Viscosity" kernel
		force += ( (fluid_viscosity_and_coef) * (particles_vel[fnr.n]- particles_vel[fnr.p]) );
		
		// Throw in the common (h-r) * 1/rho_p * 1/rho_n
		force *= h_r * 1.0f / (particles_density[fnr.p] * particles_density[fnr.n]);
		
		const D3DXVECTOR2 final_force = FluidWaterMass * force;
		// Apply force - equal and opposite to both particles
		particles_acc[fnr.p] += final_force;
		particles_acc[fnr.n] -= final_force;
	}
	//prof->EndFrame("ComputeForceFluid");
}

// Simulation Update
// Integration
void Fluid::Integrate( float dt ) 
{
	//prof->StartFrame("IntegrateFluid");
	// Walls
	//std::vector<D3DXVECTOR3> planes;		//em (made a member variable)
	//planes.push_back(D3DXVECTOR3(1, 0, 0));
	//planes.push_back(D3DXVECTOR3(0, 1, 0));
	//planes.push_back(D3DXVECTOR3(-1, 0, width));
	//planes.push_back(D3DXVECTOR3(0, -1, height));

	const D3DXVECTOR2 gravity = D3DXVECTOR2(0, 1);
	const float stiff = -FluidStaticStiff;
	for( unsigned int particle = 0 ; particle < particles_pos.size() ; ++particle )
	{
		//Particle* part = particles[particle];		//em
		D3DXVECTOR2 accumulator{ 0, 0 };
		D3DXVECTOR2& pos = particles_pos[particle];
		D3DXVECTOR2& acc = particles_acc[particle];
		D3DXVECTOR2& vel = particles_vel[particle];
		// Walls
		for( int i = 0; i< planes.size(); i++ )
		{
			const D3DXVECTOR3& p = planes[i];
			const float dist = pos.x * p.x + pos.y * p.y + p.z;
			accumulator += min(dist, 0) * stiff * D3DXVECTOR2( p.x, p.y );
		}
		acc += accumulator;
		// Acceleration
		acc += gravity;

		// Integration - Euler-Cromer		
		vel += dt * acc;
		pos += dt * vel;
		acc = D3DXVECTOR2(0, 0);
	}
	//prof->EndFrame("IntegrateFluid");
}

// Simulation Update
void Fluid::Update( float dt ) 
{
	//prof->StartFrame("UpdateFluid");

	// Pause runs the simulation standing still for profiling
	if( paused || step == pause_step ) { 
		dt = 0.0f; 
	}
	else { 
		step++; 
	}

	// Create neighbor information
	UpdateGrid();
	GetNeighbors();

	// Calculate the forces for all of the particles
	ComputeDensity();
	ComputeForce();

	// And integrate
	Integrate(dt);
	//prof->EndFrame("UpdateFluid");

	counter++;
	if (counter == 100) {
		//Formatter::Dump(//prof->GetStats());
	}
}
